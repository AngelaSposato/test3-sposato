package eclipse;

public class Recursion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] words = new String[] {"quinn", "tqest", "squirrel"};
		System.out.println(recursiveCount(words, 1));
	}
	
	public static int recursiveCount(String[] words, int n) {
		//making sure that words is always greater than n, otherwise is base case
		if (words.length>n) {
			//if condition is met...
			if (words[n].contains("q") && (n % 2 == 0 && words.length>=n)) {
				return recursiveCount(words, n+1)+1;
			}
			//when the condition is not met
			return recursiveCount(words, n+1);
		}
		//returns 0 if words.length less than n
		return 0;
	}
}


