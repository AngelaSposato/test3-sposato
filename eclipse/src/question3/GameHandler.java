//Angela Sposato 1934695
package question3;

import java.util.Random;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.text.Text;

public class GameHandler implements EventHandler<ActionEvent>{
	private TextField input;
	private Text message;
	private Text winnings;
	private Text moneyValue;
	private String choice;
	private int initialValue = 100;
	private Random randGen = new Random();
	
	public GameHandler(TextField input, Text message, Text winnings, Text moneyValue, String choice) {
		this.input = input;
		this.message = message;
		this.winnings = winnings;
		this.moneyValue = moneyValue;
		this.choice = choice;
	}
	
	public void handle(ActionEvent e) {
		//getting the value user put in
		int betValue = Integer.parseInt(input.getText());
		//randomly generating 0 or 1 to decide heads or tails
		int result = randGen.nextInt(2);
		//if result is zero value is heads
		if (result == 0) {
			message.setText("Heads");
			//checking if user put in heads or not
			if (choice.equals(message.getText())) {
				winnings.setText("You won");
				initialValue = initialValue + betValue;
				moneyValue.setText("Your money:" + String.valueOf(initialValue));
			}
			else {
				winnings.setText("You lost");
				initialValue = initialValue - betValue;
				moneyValue.setText("Your money:" + String.valueOf(initialValue));
			}
		}
		else {
			//if result is 1 then value is tails
			message.setText("Tails");
			//checking whether user chose tails
			if (choice.equals(message.getText())) {
				winnings.setText("You won");
				initialValue = initialValue + betValue;
				moneyValue.setText("Your money:" + String.valueOf(initialValue));
			}
			else {
				winnings.setText("You lost");
				initialValue = initialValue - betValue;
				moneyValue.setText("Your money:" + String.valueOf(initialValue));
			}
		}
		//validating input
		if (betValue > initialValue || input.getText().equals("")) {
			message.setText("Please enter a proper bet value");
		}
	}
}
