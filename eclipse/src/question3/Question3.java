//Angela Sposato 1934695
package question3;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;



public class Question3 extends Application {
	public void start(Stage stage) {
		Group root = new Group(); 
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);
		//Buttons for heads and tailss
		Button heads = new Button("Heads");
		Button tails = new Button("Tails");
		//TextField to input value and Text objects to display info for user
		TextField inputValue = new TextField("100");
		Text result = new Text("Result:");
		Text winnings = new Text("Winnings");
		Text moneyValue = new Text("Your money:");
		HBox hbox = new HBox();
		//creating new event handlers for each button
		GameHandler headsGame = new GameHandler(inputValue, result, winnings, moneyValue, "Heads");
		GameHandler tailsGame = new GameHandler(inputValue, result, winnings, moneyValue, "Tails");
		//setting the action
		heads.setOnAction(headsGame);
		tails.setOnAction(tailsGame);
		//adding all elements to a VBox
		hbox.getChildren().addAll(heads, tails, inputValue, result, winnings, moneyValue);
		root.getChildren().add(hbox);
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }

}
